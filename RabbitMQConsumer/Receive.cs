﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace RabbitMQConsumer
{
    class Receive
    {
        public static void Main(string[] args)
        {

            if (args.Length != 4)
            {
                WriteLineEX("Proper usage: ", 2);
                WriteLineEX("\nRabbitMQConsumer server port user password\n");
                return;
            }

            WriteLineEX("***You're Consumer***\n\n");
            WriteLineEX("***We are waiting for an incoming message to start communication.***\n\n");
            Consumer(args[0], int.TryParse(args[1], out int port) ? port : 5672, args[2], args[3]);
        }

        public static void Consumer(string server, int port, string user, string password)
        {
            var factory = new ConnectionFactory() { HostName = server, Port = port, UserName = user, Password = password };

            var connection = factory.CreateConnection();
            IModel channel = connection.CreateModel();
            channel.BasicQos(0, 1, false);
            EventingBasicConsumer eventingBasicConsumer = new EventingBasicConsumer(channel);

            eventingBasicConsumer.Received += (sender, basicDeliveryEventArgs) =>
            {
                string message = Encoding.UTF8.GetString(basicDeliveryEventArgs.Body);
                channel.BasicAck(basicDeliveryEventArgs.DeliveryTag, false);
                WriteLineEX($"Message: {message}\n", 2);
                WriteLineEX("Please response: ", 1);
                string response = Console.ReadLine();
                IBasicProperties replyBasicProperties = channel.CreateBasicProperties();
                replyBasicProperties.CorrelationId = basicDeliveryEventArgs.BasicProperties.CorrelationId;
                byte[] responseBytes = Encoding.UTF8.GetBytes(response);
                channel.BasicPublish("", basicDeliveryEventArgs.BasicProperties.ReplyTo, replyBasicProperties, responseBytes);
            };

            channel.BasicConsume("test.queues.rpc", false, eventingBasicConsumer);
        }

        static public void WriteLineEX(string text, int param = 0)
        {
            switch (param)
            {
                case 1:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                default:
                    break;
            }
            Console.Write(text);
            Console.ResetColor();
        }
    }
}

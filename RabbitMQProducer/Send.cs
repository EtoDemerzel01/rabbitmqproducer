﻿using System;
using RabbitMQ.Client;
using System.Text;
using RabbitMQ.Client.Events;

namespace RabbitMQProducer
{
    class Send
    {
        static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                WriteLineEX("Proper usage: ", 2);
                WriteLineEX("\nRabbitMQProducer server port user password\n");
                return;
            }

            WriteLineEX("***You're Producer***\n");
            WriteLineEX("***Start a conversation***\n\n");
            RunQueue(args[0], int.TryParse(args[1], out int port) ? port : 5672, args[2], args[3]);
        }

        private static void RunQueue(string server, int port, string user, string password)
        {
            var factory = new ConnectionFactory() { HostName = server, Port = port, UserName = user, Password = password };
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.QueueDeclare("test.queues.rpc", true, false, false, null);
            Producer(channel);
        }

        private static void Producer(IModel channel)
        {
            string ResponseQueue = channel.QueueDeclare().QueueName; // queue for response, auto name

            string correlationId = Guid.NewGuid().ToString();
            string response = null;

            IBasicProperties basicProperties = channel.CreateBasicProperties();
            basicProperties.ReplyTo = ResponseQueue;
            basicProperties.CorrelationId = correlationId;
            WriteLineEX("Your message: ", 1);
            string message = Console.ReadLine();
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            channel.BasicPublish("", "test.queues.rpc", basicProperties, messageBytes);

            EventingBasicConsumer EventingBasicConsumer = new EventingBasicConsumer(channel);
            EventingBasicConsumer.Received += (sender, basicDeliveryEventArgs) =>
            {
                IBasicProperties props = basicDeliveryEventArgs.BasicProperties;
                if (props != null
                    && props.CorrelationId == correlationId)
                {
                    response = Encoding.UTF8.GetString(basicDeliveryEventArgs.Body);
                }
                channel.BasicAck(basicDeliveryEventArgs.DeliveryTag, false);
                WriteLineEX($"Answer: {response}", 2);
                WriteLineEX("\nYour message: ", 1);
                message = Console.ReadLine();
                messageBytes = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish("", "test.queues.rpc", basicProperties, messageBytes);
            };
            channel.BasicConsume(ResponseQueue, false, EventingBasicConsumer);
        }

        static public void WriteLineEX(string text, int param = 0)
        {
            switch (param)
            {
                case 1:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                default:
                    break;
            }

            Console.Write(text);
            Console.ResetColor();
        }
    }


}

